const express = require('express');
const cors = require('cors');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());

app.post('/encode', (req, res) =>{
    res.send(Vigenere.Cipher(req.body.pass).crypt(req.body.encode));
});

app.post('/decode', (req, res) => {
    res.send(Vigenere.Cipher(req.body.pass).crypt(req.body.decode));
});

app.listen(port, () => {
    console.log('we are live in ' + port);
});